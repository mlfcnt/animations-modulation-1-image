var divImg = document.getElementById('affiche');
var delayInMilliseconds = 30000;

window.addEventListener('DOMContentLoaded', animation);



function animation() {
    divImg.classList.add("animated");
    divImg.classList.add("zoomIn");

    setTimeout(function () {
        divImg.classList.remove("zoomIn");
        divImg.classList.add("zoomOut");
    }, delayInMilliseconds);




}