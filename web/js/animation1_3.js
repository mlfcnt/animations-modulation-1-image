var divImg = document.getElementById('main_image');
var delayInMilliseconds = 30000;

window.addEventListener('DOMContentLoaded', animation);



function animation() {
    divImg.classList.add("animated");
    divImg.classList.add("bounceInUp");

    setTimeout(function () {
        divImg.classList.remove("bounceInUp");
        divImg.classList.add("bounceOutDown");
    }, delayInMilliseconds);
}